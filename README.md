# Hoot - Home Observation Occupant Tracker

Distributed devices project - 4th year software development IT Carlow

# Live prototype
http://hootproject.pythonanywhere.com/

# About
This is the web application that will accompany the physical HOOT device

Made using Python and Flask as well as the following libraries
- flask_login
- flask_sqlalchemy
- flask_wtf
- flask_bootstrap
- wtforms
- datetime
- jinja
- pandas
- xhtml2pdf

# Project
The aim is to create a device to assist carers of eary stage alzheimer's patients in that patients home.
To do this we have created a device using a Raspberry Pi that is connected to infrared sensors.
When a sensor is triggered the location of that sensor and the datetime is sent to a database where reports
are created and sent to the carers. Reports may also be viewed via the web app.
The aim is that by providing additional information to the carers we can make their job easier
and help them understand more about their patient.

# Team members
- Ronan Donohue
- Neil Grogan
- Ailish Kavanagh
- Erik Prananta