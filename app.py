from flask import Flask, render_template, request, session, flash, redirect, url_for
from flask_login import LoginManager, UserMixin, \
                                login_required, login_user, logout_user, current_user
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from flask_bootstrap import Bootstrap
from wtforms import StringField, PasswordField
from wtforms.fields.html5 import DateField
from wtforms.validators import InputRequired, Length, Email

from datetime import date
import jinja2
import pandas as pd
from xhtml2pdf import pisa

app = Flask(__name__)

app.config['SECRET_KEY'] = 'erikisgay'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://username:password@localhost/db_name'
Bootstrap(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"

db = SQLAlchemy(app)

#define the user table and its columns
class User(UserMixin, db.Model):
        id = db.Column(db.Integer, primary_key=True)
        username = db.Column(db.String(30), unique=True)
        email =  db.Column(db.String(50), unique=True)
        password =  db.Column(db.String(40))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

#define forms and their fields that will display for signup and login
class loginForm(FlaskForm):
    username = StringField('USERNAME', validators=[InputRequired(), Length(min=6, max=15)])
    password = PasswordField('PASSWORD', validators=[InputRequired(), Length(min=8, max=30)])

class signupForm(FlaskForm):
    username = StringField('USERNAME', validators=[InputRequired(), Length(min=6, max=15)])
    email = StringField('EMAIL', validators=[InputRequired(), Email(message='Invalid email'), Length(max=40)])
    password = PasswordField('PASSWORD', validators=[InputRequired(), Length(min=8, max=30)])

class dateForm(FlaskForm):
    startDate = DateField('START DATE', format='%Y-%m-%d')
    endDate = DateField('END DATE', format='%Y-%m-%d')


@app.route('/')
def home():
    return render_template('index.html', 
                            title='Welcome to HOOT')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = loginForm()

    if form.validate_on_submit():
        #search for user in db
        user = User.query.filter_by(username=form.username.data).first()
        if user:
            if user.password == form.password.data:
                login_user(user)
                return redirect(url_for('welcome'))
        #return 'Invalid login details'


    return render_template('login.html',
                            title='Login',
                            form = form)



@app.route('/signup', methods=['GET', 'POST'])
def signup():

    form = signupForm()

    if form.validate_on_submit():
        #create new user object and add to device
        new_user = User(username = form.username.data, email = form.email.data, password = form.password.data)
        db.session.add(new_user)
        db.session.commit()

    return render_template('signup.html',
                        title='Sign Up',
                        form = form)


@app.route('/welcome')
@login_required
def welcome():

    return render_template('welcome.html',
                        title='Successful Login',
                        name = current_user.username)

@app.route('/report', methods=['GET', 'POST'])
#@login_required
def report():
    # 1: have email stored as session variable
    # 2: let user enter a date range
    # 3: find the entries in the db that fit that range and email
    # 4: put returned data in a panda
    # 5: send panda to html file for display

    form = dateForm()

    if form.validate_on_submit():
        #TO-DO
        return render_template('report.html',
                                title="Viewing Reports",
                                form = form)

    return render_template('report.html',
                        title='Select a date range to view reports',
                        form = form)

@app.route("/logout")
@login_required
def logout():

    logout_user()
    return redirect(url_for('home'))


if __name__ == '__main__':
    app.run(debug=True)
